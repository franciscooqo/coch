<?php
    /*template Name: Clubes */
    get_template_part('includes/header');
    b4st_main_before();
    
?>

<?php if(have_posts()): while(have_posts()): the_post(); ?>
<div class="container">
  <div class="row">
    <div class="">
      <?php the_content();?>
    </div>
  </div>
</div>
<?php endwhile; 
wp_reset_postdata();
endif;
?>
<main id="main" class="container pt-2">
  <div class="row pt-5">
    <select name="clubes-regiones" id="clubes-regiones-selector" class="mt-5 mb-5">
      <option value="0">Seleccionar Región</option>
      <option value="tarapaca">I Tarapacá</option>
      <option value="antofagasta">II Antofagasta</option>
      <option value="atacama">III Atacama</option>
      <option value="coquimbo">IV Coquimbo</option>
      <option value="valparaiso">V Valparaiso</option>
      <option value="ohiggins">VI </option>
      <option value="maule">VII</option>
      <option value="concepcion">VIII</option>
      <option value="araucania">IX</option>
      <option value="lagos">X</option>
      <option value="aysen">XI</option>
      <option value="magallanes">XII</option>
      <option value="metropolitana">RM</option>
      <option value="rios">XIV</option>
      <option value="arica">XV</option>
      <option value="nuble">XVI</option>
    </select>
  </div>
      <?php $clubes = new WP_Query(array(
        'post_type'       => array('clubes'),
        'posts_per_page'   => -1,
        'post_status'     => 'publish',
        'orderby'         => 'name',
        'order'           => 'ASC',
      ));
      if( $clubes->have_posts() ) : ?> 
  <div class="row ch-federados ch-federados--cards">
    <?php while($clubes->have_posts()) : $clubes->the_post(); ?>
    <div class="col-sm-3">
        <a href="#"
          class="d-none ch-federados--card
          <?php 
            $terms = get_the_terms( $post->ID , 'clubestax' );
            
            foreach( $terms as $term ) {
              $field = get_field_object('region_club', $term->taxonomy.'_'.$term->term_id);
              $value = $field['value'];
              echo $value['value'].' ';
            }  ?>"
          data-cardname="<?php 
            $terms = get_the_terms( $post->ID , 'clubestax' );
            foreach( $terms as $term ) {
              $field = get_field_object('region_club', $term->taxonomy.'_'.$term->term_id);
              $value = $field['value'];
              echo $value['value'].' ';
            }?>" >
          <div class="sombreado rounded club-card
          <?php
            // vars	
            $regiones = get_field('region_club');

            // check
            if( $regiones ): ?>
            <?php echo implode(', ', $regiones);?>
            <?php the_field('regiones'); ?>
            <?php endif; ?>"
          >
            <img class="w-100" src="<?php the_post_thumbnail_url();?>" alt="<?php the_title();?>">        
            <h4 class="text-center"><?php the_title();?></h4>
          </div>
          
        </a>
        </div> 
      <?php endwhile ;
        wp_reset_postdata();
        endif; ?>
  </div>


  <div class="row d-none ch-federados ch-federados--info">
    <div class="col-6">
      <img src="" alt="" class="w-100 ch-federados--img">
    </div>
    <?php 
    $taxonomy = 'clubestax';
    $ars = array(
      'orderby' => 'name',
      'hide_empty' => false,
    );
    $cats = get_terms( $taxonomy, $ars );
    foreach ($cats as $cat):
    $cat_id= $cat->term_id;
    $field = get_field_object('region_club', $cat->taxonomy.'_'.$cat->term_id);
    $value = $field['value'];
    ?>
    <div class="col-6 ch-federados--t">
      <div class="d-none club-card--tax ch-federados--text" id="<?php echo $value['value'].' ';?>">
          <p>
          <br>
          Nombre:<br><span><?php echo $cat->name ?></span>
          <br><br>
          Comuna:<br><span><?php the_field('comuna_club', $cat->taxonomy.'_'.$cat->term_id);?></span>
          <br><br>
          Región:<br><span><?php echo $value['label'];?></span>
          <br><br>
          Dirección:<br><span><?php the_field('direccion', $cat->taxonomy.'_'.$cat->term_id);?></span>
          <br><br>
          Teléfono:<br><span><?php the_field('telefono_club', $cat->taxonomy.'_'.$cat->term_id);?></span>
          <br><br>
          Correo:<br><span style="color:black;"><?php the_field('correo_club', $cat->taxonomy.'_'.$cat->term_id);?></span>
          </p>
      </div>
    </div>
    <?php endforeach;?>
  </div>
  <hr>

</main><!-- /.container -->
<?php 
    b4st_main_after();
    get_template_part('includes/footer'); 
?>
