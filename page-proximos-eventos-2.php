<?php
    /*template Name: Clubes */
    get_template_part('includes/header');
    b4st_main_before();
?>

<?php if(have_posts()): while(have_posts()): the_post(); ?>
<div class="container">
  <div class="row">
    <div class="">
      <?php the_content();?>
    </div>
  </div>
</div>
<?php endwhile; 
wp_reset_postdata();
endif;
?>
<main id="main" class="w-100 mt-0 mb-4 ml-auto mr-auto">
  <div class="row">
    
      <!-- MEGA EVENTOS -->
<?php
$mega_eventos = new WP_Query(array(
'post_type' 		=> array('mega_eventos'),
'posts_per_page'	=> -1,
'post_status'		=> 'publish',
'orderby' 			=> 'menu_order date',
'order'   			=> 'ASC',
));
if ( $mega_eventos->have_posts() ) : ?>
<div id="iconos-eventos" class="col-12 col-md-10 ml-auto mr-auto pl-5 pr-5 pt-4 pb-4">
  <div class="bg-light rounded sombreado contenedor-sombreado row w-100 p-0 m-0 owl-carousel owl-carousel-eventos owl-theme">
    <?php while ( $mega_eventos->have_posts() ) : $mega_eventos->the_post();?>
      <div class="d-inline-block justify-content-center ml-auto mr-auto">
        <?php $logo_del_evento = get_field( 'logo_del_evento' ); ?>
        <?php if ( $logo_del_evento ) { ?>
	        <img class="ml-auto mr-auto" style="" src="<?php echo $logo_del_evento['url']; ?>" alt="<?php echo $logo_del_evento['alt']; ?>"/>
        <?php } ?>
        <p class="text-center texto-evento">Comienza en:<br><span class="clocks" style="color:red;font-weight:bolder;" data-countdown="<?php the_field( 'comienza_el' ); ?>"></span></p>
      </div>
    <?php endwhile;?>
  </div>
</div>
<?php endif;?>
<?php wp_reset_postdata(); ?>

</main><!-- /.container -->
<?php 
    b4st_main_after();
    get_template_part('includes/footer'); 
?>
